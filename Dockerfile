FROM ubuntu:18.04

RUN 	apt-get update && \

	apt-get install -y --no-install-recommends \
		tzdata && \
	echo "Europe/Zurich" > /etc/timezone && \
	rm -f /etc/localtime && \
	dpkg-reconfigure  -f noninteractive tzdata && \

	apt-get -y install --no-install-recommends \
		ca-certificates && \
    	apt-get -y --force-yes install --no-install-recommends \
		xorg-dev && \
   	apt-get install -y --no-install-recommends \
		mesa-utils && \
    	apt-get install -y --no-install-recommends \ 
		build-essential 		\
		cmake 				\
		libogre-1.9-dev 		\
		ogre-1.9-tools 			\
		libenet-dev 			\
		libvorbis-dev 			\
		libalut-dev 			\
		libcegui-mk2-dev 		\
		tcl-dev 			\
		libboost-date-time-dev 		\
		libboost-filesystem-dev 	\
		libboost-thread-dev 		\
		liblua5.1-0-dev 		\
		subversion 			\
		libtolua-dev 			\
		libtolua++5.1-dev 		\
		zsh curl git 			&& \
    	
	rm -rf /var/lib/apt/lists/*		&& \

	curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | zsh || true

WORKDIR /var/orxonox_d
CMD /bin/zsh
