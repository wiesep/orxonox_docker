# Orxonox Docker
## Description
This repository provides a Dockerfile, Build- and Runscript to compile and run Orxonox inside a docker container. Therefore Orxonox should compile and be playable on every host system.

## How to use orxonox-docker
1. Install docker for your OS  
https://tuleap-documentation.readthedocs.io/en/latest/developer-guide/quick-start/install-docker.html#install-docker-engine

2. Download orxonox  
https://www.orxonox.net/wiki/pps/download

3. Download Dockerfile and build the Image  
Replace <orxonox_docker> with the path to your orxonox download folder.
```console
foo@host:~$ cd <orxonox_folder>
foo@host:~$ git clone git@gitlab.ethz.ch:wiesep/orxonox_docker.git docker
foo@host:~$ cd docker && ./build.sh
```

4. Edit run script and start container
```console
foo@host:~$ nano run.sh
```
Adjust the orxonox installation folder
```dockerfile
# path to directory containing orxonox source files
ORXONOX_SRC="<orxonox_folder"
```
```console
foo@host:~$ ./run.sh
```

5. Compile and run orxonox  
To compile the game enter:
```console
foo@docker:~$ mkdir build && cd build
foo@docker:~$ cmake ../trunk
foo@docker:~$ make -j2
```
To run the game
```console
foo@docker:~$ ./run
```

## Developing  

Now you can edit the source code and assets of the game from your host OS with any program you like. To recompile and run the game just start a orxonox docker container and rebuild the source code as following:

To compile the game enter:
```console
foo@host:~$ cd <orxonox_folder>/docker && ./run.sh
foo@docker:~$ mkdir build && cd build
foo@docker:~$ make -j2
```

## Author
Philip Wiese
<wiesep@student.ethz.ch>  
D-ITET
