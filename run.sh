#!/bin/bash

# set up x-session for docker image user
XSOCK="/tmp/.X11-unix"
XAUTH="/tmp/.docker.xauth"
touch "$XAUTH"
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f "$XAUTH" nmerge -

# path to directory containing orxonox source files
ORXONOX_SRC="/home/xeratec/Projects/orxonox"

# in order: mappings for xorg, orxonox source files and pulse audio
docker run \
	-v "$XSOCK":"$XSOCK":rw \
	-v "$XAUTH":"$XAUTH":rw \
	--device=/dev/dri/card0:/dev/dri/card0 \
	--device=/dev/snd:/dev/snd \
	-e DISPLAY=$DISPLAY \
	-e XAUTHORITY="$XAUTH" \
        \
        -v "$ORXONOX_SRC":/var/orxonox_d \
        \
        -v /dev/shm:/dev/shm \
        -v /etc/machine-id:/etc/machine-id \
        -v /run/user/$(id -u $USER)/pulse:/run/user/$(id -u $USER)/pulse \
        -v /var/lib/dbus:/var/lib/dbus \
        -v ~/.pulse:/home/$USER/.pulse \
        --rm \
	-it \
        orxonox
